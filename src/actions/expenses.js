import database from '../firebase/firebase';

// ADD_EXPENSE
// Add new expense to Redux store
export const addExpense = (expense) => ({
    type: 'ADD_EXPENSE',
    expense,
});

// Writes expense to Firebase and then, on expense, updates Redux store.
export const startAddExpense = (expenseData = {}) => {
    return (dispatch, getState) => {
        // set default values when passed empty object
        const {
            description = '',
            note = '',
            amount = 0,
            createdAt = 0
        } = expenseData;
        // Get values from parameters
        const uid = getState().auth.uid;
        const expense = { description, note, amount, createdAt };


        return database.ref(`users/${uid}/expenses`).push(expense)   // Write to Firebase (Returns Promise with reference to child location)
            .then((ref) => { // Once Firebase reports success, update the Redux store
                dispatch(addExpense({
                    id: ref.key,
                    ...expense
                }));
            });
    };
};

// REMOVE_EXPENSE
// Remove expense from Redux store
export const removeExpense = ({ id } = {}) => ({
    type: 'REMOVE_EXPENSE',
    id,
});

// Remove expense from Firebase
export const startRemoveExpense = ({ id } = {}) => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database.ref(`users/${uid}/expenses/${id}`).remove()
            .then(() => {
                dispatch(removeExpense({ id }))
            });
    };
};

// EDIT_EXPENSE
// Update expense in Redux store
export const editExpense = (id, updates) => ({
    type: 'EDIT_EXPENSE',
    id,
    updates,
});

// Update expense in Firebase
export const startEditExpense = (id, updates) => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database.ref(`users/${uid}/expenses/${id}`).update(updates)
            .then(() => {
                dispatch(editExpense(id, updates));
            });
    }
};

// SET_EXPENSES
export const setExpenses = (expenses) => ({
    type: 'SET_EXPENSES',
    expenses,
});

export const startSetExpenses = () => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database.ref(`users/${uid}/expenses`).once('value')
            .then((snapshot) => {
                const expenses = [];

                snapshot.forEach((childSnapshot) => {
                    expenses.push({
                        id: childSnapshot.key,
                        ...childSnapshot.val()
                    });
                });

                dispatch(setExpenses(expenses));
            });
    };
};