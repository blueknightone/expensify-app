import authReducer from '../../reducers/auth';

test('should add uid to auth store', () => {
    const action = {
        type: 'LOGIN',
        uid: '123abd',
    };
    const state = authReducer({}, action);
    expect(state.uid).toEqual(action.uid);
});

test('should return empty object on logout', () => {
    const action = { type: 'LOGOUT' };
    const state = authReducer({ uid: '123abc' }, action);
    expect(state).toEqual({})
});