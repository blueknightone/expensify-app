import {createStore} from 'redux';

// Actions generators - functions that return action objexts
const incrementCount = ({ incrementBy = 1 } = {}) => ({
    type: 'INCREMENT',
    incrementBy,
});

const decrementCount = ({ decrementBy = 1}= {}) => ({
    type: 'DECREMENT',
    decrementBy,
});

const setCount = ({count} = {}) => ({
    type: 'SET',
    count,
});

const resetCount = () => ({
    type: 'RESET',
});

/*Redux Reducer - Specifies how data changes in response to the Action*/
// 1. Reducers are pure functions output is only determined by input (doesn't interact with anything outside of the function scope)
// 2. Never changes state or action, reads them and returns an object that represents the new state.
const countReducer = (state = { count: 0 }, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return { count: state.count + action.incrementBy };
        case 'DECREMENT':
            const decrementBy = action.decrementBy;
            return { count: state.count - decrementBy };
        case 'RESET':
            return { count: 0 };
        case 'SET':
            return { count: action.count };
        default:
            return state;
    }
};

const store = createStore(countReducer);

const unsubscribe = store.subscribe(() => {
    console.log(store.getState())
});

// Actions - object that gets sent to the store. Describes the change in the data and the data.
/*store.dispatch({
    type: 'INCREMENT',
    incrementBy: 5,
});*/

store.dispatch(incrementCount({ incrementBy: 5 }));

store.dispatch(incrementCount());

store.dispatch(resetCount());

store.dispatch(decrementCount());

store.dispatch(decrementCount({ decrementBy: 10 }));

store.dispatch(setCount({ count: 101 }));

unsubscribe();