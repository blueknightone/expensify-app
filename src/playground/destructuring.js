
// Object Destructuring

// const person = {
//     // name: 'Justin',
//     age: 33,
//     location: {
//         city: 'Lovelock',
//         temp: 95,
//     }
// };
//
// const {name = 'Anonymous', age} = person;
//
// console.log(`${name} is ${age}.`);
//
// const {city, temp: temperature = '50'} = person.location;
//
// if (city && temperature) {
//     console.log(`It's ${temperature} in ${city}.`);
// }

// const book = {
//     title: 'Ego is the Enemy',
//     author: 'Ryan Holiday',
//     publisher: {
//         // name: 'Penguin',
//     },
// };
//
// const {name: publisherName = 'Self-published'} = book.publisher;
//
// console.log(publisherName);

// Array Destructuring

// const address = ['1299 S Juniper Street', 'Philadelphia', 'Pennsylvania', '19147'];
//
// const [, city, state = 'Nevada'] = address;
//
// console.log(`You are in ${city}, ${state}.`);

const item = ['Coffee (iced)', '$2.00', '$3.50', '2.75'];

const [product, , mediumPrice] = item;

console.log(`A medium ${product} costs ${mediumPrice}`);
