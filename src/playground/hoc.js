// Higher Order Component (HOC) - A React component (HOC) that renders another component.
// Reuse code through render hijacking, prop manipulation, and abstracting staate

import React from 'react';
import ReactDOM from 'react-dom';

const Info = (props) => {
    return (
        <div>
            <h1>Info</h1>
            <p>The info is: {props.info}</p>
        </div>
    );
};

// HOC
const withAdminWarning = (WrappedComponent) => {
    return (props) => (
        <div>
            {props.isAdmin && <p>This is private information. Please don't share.</p>}
            <WrappedComponent {...props} />
        </div>
    );
};

// requireAuthentication HOC
const requireAuthentication = (WrappedComponent) => {
    return (props) => (
        <div>
            {props.isAuthenticated ? <WrappedComponent {...props} /> : <p>Please log in to view the info.</p>}
        </div>
    );
};

const AdminInfo = withAdminWarning(Info);
const AuthInfo = requireAuthentication(AdminInfo);

ReactDOM.render(<AuthInfo isAuthenticated={true} isAdmin={true} info='These are the details.' />, document.getElementById('app'));

