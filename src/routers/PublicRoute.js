import React from 'react';
import { Redirect, Route } from "react-router-dom";
import connect from "react-redux/es/connect/connect";

export const PublicRoute = ({ isAuthenticated, component: Component, ...rest, }) => (
    <Route
        {...rest}
        component={(props) => (
            isAuthenticated ? (
                <Redirect to='/dashboard' />
            ) : (
                <div>
                    <Component {...props} />
                </div>
            )
        )}
    />
);

const mapStateToProps = (state) => ({
    isAuthenticated: !!state.auth.uid,
});

export default connect(mapStateToProps)(PublicRoute);

