import React from 'react';

const LoadingPage = () => (
        <div className='loader'>
            <img className='loader__image' src="/images/loader.gif" alt="Loading Expensify App" />
        </div>
    );

export default LoadingPage;
