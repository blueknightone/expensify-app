import React from "react";
import { connect } from 'react-redux';
import { startLogout } from "../actions/auth";
import { Link } from "react-router-dom";

export const Header = (props) => (
    <header className='header'>
        <div className='content-container'>
            <div className='header__content'>
                <Link to='/' className='header__title'>
                    <h1>Expensify</h1>
                </Link>
                <button className='button button--link' onClick={props.startLogout}>Log Out</button>
            </div>
        </div>
    </header>
);

const mapStateToProps = (state) => {
    return ({
        displayName: state.auth.displayName,
    });
};

const mapDispatchToProps = (dispatch) => ({
    startLogout: () => dispatch(startLogout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
